(function () {
  'use strict';

  angular
    .module('app')
    .factory('dataservice', dataservice);

  /* @ngInject */
  function dataservice($http, $httpParamSerializerJQLike, $q) {
    $http.defaults.transformRequest.unshift($httpParamSerializerJQLike);

    var readyPromise;
    var httpHost = '/';

    var service = {
      ready: ready,
      getPrimedData: getPrimedData,
      postForm: postForm,
    };

    return service;

    ////////////

    function getPrimedData() {
      return $http.get(httpHost + 'users/list')
        .then(complete)
        .catch(failed);

      function complete(response) {
        return response.data;
      }

      function failed(response) {
        return $q.reject(response);
      }
    }

    function postForm(data) {
      return $http.post(httpHost + 'user', data)
        .then(complete)
        .catch(failed);

      function complete(response) {
        return response.data;
      }

      function failed(response) {
        return $q.reject(response);
      }
    }

    function getReady() {
      if (!readyPromise) {
        readyPromise = $q.when(getPrimedData());
      }

      return readyPromise;
    }

    function ready(promisesArray) {
      return getReady()
        .then(function () {
          return promisesArray ? $q.all(promisesArray) : readyPromise;
        });
    }

  }
})();
