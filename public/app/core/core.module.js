(function () {
  'use strict';

  angular.module('app.core', [
     /* Angular modules */
    'ngSanitize',
    'ngTouch',
    'ngMessages',
  ]);
})();
