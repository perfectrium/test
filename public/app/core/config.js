(function () {
  'use strict';

  var core = angular.module('app.core');

  var config = {};

  core.value('config', config);

  core.config(configure);

  /* @ngInject */
  function configure($compileProvider, $logProvider, $interpolateProvider, $locationProvider, $httpProvider, $animateProvider) {
    $compileProvider.debugInfoEnabled(false);

    // turn debugging off/on (no info or warn)
    if ($logProvider.debugEnabled) {
      $logProvider.debugEnabled(true);
    }

    $interpolateProvider
      .startSymbol('[[')
      .endSymbol(']]');

    // $locationProvider.html5Mode(true).hashPrefix('!');

    // Set X-Requested-With header
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
  }
})();
