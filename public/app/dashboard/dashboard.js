(function () {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('DashboardController', DashboardController);

  function DashboardController($scope, $location, $element, $window, $q, $timeout, dataservice) {
    var vm = this;

    /**
     * Form default values
     * @type {Object}
     */
    vm.data = {};
    vm.formErrors = [];

    // Set true when data sending
    vm.formSubmitting = false;
    vm.submit = submit;
    vm.users = 0;

    dataservice.ready().then(function (users) {
      vm.users = users;
    });

    function submit(formName) {
      // clean up server errors
      vm.formErrors = [];

      showProcessing();

      dataservice.postForm(vm.data)
        .then(function (response) {
          if (response.isSuccess) {
            vm.users.push(JSON.parse(JSON.stringify(vm.data)));
          } else {
            vm.formErrors.push(response.msg);
            console.log(vm.formErrors);
          }
        })
        .catch(function (response) {
          hideProcessing();
        });
    }

    //////////

    function showProcessing() {
      vm.formSubmitting = true;
    }

    function hideProcessing() {
      vm.formSubmitting = false;
    }
  }
})();
