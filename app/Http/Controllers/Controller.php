<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    public function index()
    {
        return view('index', ['user' => '']);
    }
    
    public function store(Request $request)
    {
        $fname = $request->input('first_name');
        $lname = $request->input('last_name');
        
        if (empty($fname) || empty($lname)) {
            return response()->json(['isSuccess' => false, 'msg' => 'Please fill in all fields']);
        }
        
        $result = DB::insert('insert into users (first_name, last_name) values (?, ?)', [(string)$fname, (string)$lname]);
        
        return response()->json(['isSuccess' => $result]);
    }

    public function getList()
    {
        $results = DB::select("SELECT first_name, last_name FROM users");

        return response()->json($results);
    }
}
