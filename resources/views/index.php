<html ng-app="app">
    <head>
        <meta charset="UTF-8" />

        <link href="/vendor_js/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="/vendor_js/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        
        <style>
            .ng-hide {display: none !important; }
        </style>
    </head>
    <body>
        <div ng-controller="DashboardController as vm">
            
            <div class="container" style="margin-top: 20px;">
                <form ng-submit="vm.submit()" name="form" method="post" novalidate="" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-xs-3 col-md-2 control-label required" for="firstName">First name</label>
                        <div class="col-xs-9 col-md-8">
                            <input type="text" ng-model="vm.data['first_name']" id="firstName" name="first_name" required="required" maxlength="255" class="form-control" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 col-md-2 control-label required" for="lastName">Last name</label>
                        <div class="col-xs-9 col-md-8">
                            <input type="text" ng-model="vm.data['last_name']" id="lastName" name="last_name" required="required" maxlength="255" class="form-control" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">Add</button>
                        </div>
                    </div>
                    
                    <div class="alert alert-danger ng-hide" ng-show="vm.formErrors" ng-repeat="error in vm.formErrors">
                        [[ error ]]
                    </div>
                </form>
            </div>
            
            <ul>
                <li data-ng-repeat="user in vm.users">
                    [[user.first_name]] [[user.last_name]]
                </li>
            </ul>

        </div>

        <script src="/vendor_js/angular/angular.js"></script>
        <script src="/vendor_js/angular-touch/angular-touch.min.js"></script>
        <script src="/vendor_js/angular-messages/angular-messages.min.js"></script>
        <script src="/vendor_js/angular-sanitize/angular-sanitize.js"></script>
        <script src="/app/app.module.js"></script>
        <script src="/app/core/core.module.js"></script>
        <script src="/app/core/config.js"></script>
        <script src="/app/core/dataservice.js"></script>
        <script src="/app/dashboard/dashboard.module.js"></script>
        <script src="/app/dashboard/dashboard.js"></script>
    </body>
</html>