#!/usr/bin/env bash

#http://www.thisprogrammingthing.com/2013/getting-started-with-vagrant/

apt-get update
apt-get install debconf-utils
apt-get install -y wget
apt-get install -y apache2
apt-get install -y php5 php5-mcrypt php5-xdebug php5-curl php5-mysql php5-pgsql php5-imagick php5-gd php5-intl php-apc php5-gmp

# ------------------------------------ Composer ------------------------------------
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
# ------------------------------------ Composer [end] ------------------------------------


# ------------------------------------ Overriding default PHP settings ------------------------------------
sh -c 'echo "
display_errors = On
html_errors = 1
upload_max_filesize = 400M
post_max_size = 400M
date.timezone = "Europe/Kiev"
short_open_tag = Off

session.gc_maxlifetime = 21600

xdebug.max_nesting_level = 256
xdebug.var_display_max_depth = 5
xdebug.remote_enable = 1
xdebug.show_local_vars = 1
" >> /etc/php5/conf.d/local.ini'
# ------------------------------------ Overriding default PHP settings [end] ------------------------------------

# ------------------------------------ MySQL ------------------------------------
debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'
apt-get install -y mysql-server

# Разрешить удаленные соединения к mysql
echo "Updating mysql configs in /etc/mysql/my.cnf."
sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf
echo "Updated mysql bind address in /etc/mysql/my.cnf to 0.0.0.0 to allow external connections."

# Удаленный доступ пользователю
mysql --user=root --password=vagrant -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'vagrant' WITH GRANT OPTION;"

service mysql restart


# --------- phpMyAdmin ---------
debconf-set-selections <<< 'phpmyadmin phpmyadmin/dbconfig-install boolean true'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/app-pass password vagrant'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/app-password-confirm password vagrant'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/admin-pass password vagrant'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'
apt-get install -y phpmyadmin
# --------- phpMyAdmin [end] ---------


# Apache
if [ ! -h /var/www ];
then
    rm -rf /var/www
    ln -fs /vagrant /var/www

    a2enmod rewrite
    a2enmod ssl

    sed -i '/AllowOverride None/c AllowOverride All' /etc/apache2/sites-available/default
    sed -i '/Include\/etc\/phpmyadmin\/apache.conf/c 111' /etc/apache2/apache2.conf
    sed -e 's/Include\/etc\/phpmyadmin\/apache.conf/111/g' /etc/apache2/apache2.conf
fi


# ------------------------------------ Configuration ------------------------------------
mysql --user=root --password=vagrant -e "CREATE DATABASE test CHARACTER SET utf8 COLLATE utf8_general_ci;"
mysql --user=root --password=vagrant -e "USE test; CREATE TABLE users (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"

# Создаем папку для логов
mkdir /var/www/httpd-logs

sh -c 'echo "
<VirtualHost *:80>
    ServerName test.com
    ServerAlias www.test.com
    DocumentRoot /var/www/public

    <Directory /var/www/public>
        AllowOverride All
        Order Allow,Deny
        Allow from All
    </Directory>

    # uncomment the following lines if you install assets as symlinks
    # or run into problems when compiling LESS/Sass/CoffeScript assets
    # <Directory /var/www>
    #     Options FollowSymlinks
    # </Directory>

	ErrorLog /var/www/httpd-logs/error.log
</VirtualHost>
" >> /etc/apache2/sites-available/site.conf'


# Apache 2.2
#a2dissite default
#a2ensite site

# Apache 2.4
a2dissite 000-default
a2ensite site.conf

/etc/init.d/apache2 restart
# ------------------------------------ Configuration [end] ------------------------------------
